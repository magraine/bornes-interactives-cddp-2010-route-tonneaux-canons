<?php

function action_definir_document_logo_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	list($doc, $id_document, $objet, $id_objet) = explode('/', $arg);

	if (!$id_document = intval($id_document)
	or !$objet = objet_type($objet)
	or !$id_objet = intval($id_objet)) {
		include_spip('inc/minipres');
		minipres("action_definir_document_logo $arg pas compris");
	}

	if ($id_document) {
		// enlever le precedent logo...
		sql_updateq('spip_documents_liens', array('ligature'=>''), array(
			'objet = ' . sql_quote($objet),
			'id_objet = ' . sql_quote($id_objet),
			'ligature = ' . sql_quote('logo')
		));		
		// mettre a jour la ligature
		sql_updateq('spip_documents_liens', array('ligature' => 'logo'), array(
			'id_document = ' . sql_quote($id_document),
			'objet = ' . sql_quote($objet),
			'id_objet = ' . sql_quote($id_objet)
		));

		if ($redirect = _request('redirect')) {
			set_request('redirect', parametre_url($redirect, 'var_mode', 'calcul'));
		}
	}

}

?>
