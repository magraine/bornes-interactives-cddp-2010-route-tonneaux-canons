<?php

function action_supprimer_logo_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	list($objet, $id_objet) = explode('/', $arg);
	if (!$objet = objet_type($objet) or !$id_objet = intval($id_objet)) {
		include_spip('inc/minipres');
		minipres("action_supprimer_logo_document $arg pas compris");
	}
	
	$_id_objet = id_table_objet($objet);
	$chercher_logo = charger_fonction('chercher_logo', 'inc');
	list($addr_logo, $dir) = $chercher_logo($id_objet, $_id_objet);
	if ($addr_logo) {
		$nom = substr($addr_logo, strlen($dir));
		
		// supprimer le logo
		include_spip('action/iconifier');
		action_spip_image_effacer_dist($nom);
		if ($redirect = _request('redirect')) {
			set_request('redirect', parametre_url($redirect, 'var_mode', 'calcul'));
		}
	}

}

?>
