<?php

function action_migrer_logo_document_dist() {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	list($objet, $id_objet) = explode('/', $arg);
	if (!$objet = objet_type($objet) or !$id_objet = intval($id_objet)) {
		include_spip('inc/minipres');
		minipres("action_migrer_logo_document $arg pas compris");
	}
	$_id_objet = id_table_objet($objet);
	$chercher_logo = charger_fonction('chercher_logo', 'inc');
	list($addr_logo, $dir) = $chercher_logo($id_objet, $_id_objet);
	if ($addr_logo) {
		$nom = substr($addr_logo, strlen($dir));
		
		$file = array(
			'name' => $nom,
			'tmp_name' => $addr_logo,
			'titrer' => true,
		);


		include_spip('action/ajouter_documents'); // plugin gestion_documents (mediatheque)
		$ajouter_un_document = charger_fonction('ajouter_un_document', 'action');
		$id_document = $ajouter_un_document(0, $file, $objet, $id_objet, 'image');

		if ($id_document) {
			// mettre a jour la ligature
			sql_updateq('spip_documents_liens', array('ligature'=>'logo'), array(
				'id_document = ' . sql_quote($id_document),
				'objet = ' . sql_quote($objet),
				'id_objet = ' . sql_quote($id_objet)
			));

			// supprimer le logo
			include_spip('action/iconifier');
			action_spip_image_effacer_dist($nom);

			if ($redirect = _request('redirect')) {
				set_request('redirect', parametre_url($redirect, 'var_mode', 'calcul'));
			}
		}
	}

}

?>
